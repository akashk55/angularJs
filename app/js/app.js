var vcApp = angular.module('vendorcreation',['ngMaterial', 'ngAnimate', 'ngAria', 'ui.router', 'ngMessages' , 'md-steppers']);
    //------ Routing
vcApp.config(function ($stateProvider, $urlRouterProvider,$mdThemingProvider){
	$urlRouterProvider.otherwise("/vendorDetails");

    $mdThemingProvider.definePalette('white', {
        '50': 'ffffff',
        '100': 'ffffff',
        '200': 'ffffff',
        '300': 'ffffff',
        '400': 'ffffff',
        '500': 'ffffff',
        '600': 'ffffff',
        '700': 'ffffff',
        '800': 'ffffff',
        '900': 'ffffff',
        'A100': 'ffffff',
        'A200': 'ffffff',
        'A400': 'ffffff',
        'A700': 'ffffff',
        'contrastDefaultColor': 'light', // whether, by default, text (contrast)
        // on this palette should be dark or light

        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
            '200', '300', '400', 'A100'
        ],
        'contrastLightColors': undefined // could also specify this if default was 'dark'
    });
	//------ Configuring Theme
$mdThemingProvider.theme('default')
	.primaryPalette('indigo')
	.accentPalette('green', {
		'default': '500'
	})
	.warnPalette('red')
	.backgroundPalette('grey');

//------ Configuring Theme
$mdThemingProvider.theme('dark')
	.primaryPalette('white')
	.accentPalette('grey')
	.warnPalette('amber').dark();
		//------ Routing
	$urlRouterProvider.otherwise("/vendorDetails");

	$stateProvider
		.state('vendorDetails', {
			url: "/vendorDetails",
			templateUrl: "app/views/vendorDetails.html",
			controller: "vendorDetailsController",
			controllerAs: "VDC"
		})
});
