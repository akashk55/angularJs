vcApp.factory('$vendorFactories',function($http){
	var factory={}
	var vendorURL="https://uat-vendor-ws.bdt.kpit.com/";

	//var vendorURL = "http://10.10.177.65:1111/"

	factory.getCountryList = function(){
		return $http({
			method : 'GET',
			url : vendorURL+'comman/WBCall_GetCountryList'
		})
	}

	factory.getStateList = function(payload){
		return $http({
			method : 'POST',
			url : vendorURL+'comman/WBCall_GetStateList',
			data : payload
		})
	}

	factory.getGSTClass = function(){
		return $http({
			method : 'GET',
			url : vendorURL+'comman/WBCall_GetGstClass'

		})
	}

	factory.setVendorDetails = function(payload){
		return $http({
			method : 'POST',
			url : vendorURL+'vendor/WBCall_SetVendorDetail',
			data : payload
		})
	}
	return factory;
});
