vcApp.controller('vendorDetailsController',function($scope,$vendorFactories,$mdToast,$mdDialog){
	console.log("controller");
	var self = this;
	self.selected = 0;
	self.disableAll = false;
	//self.vendorDetails=[];
	//self.vendorDetails.countryList=[];
	//$scope.step1.completed = false;
//
// //selected=1
//
	self.stageCompleted = function(){
		console.log("Stages completed");
		self.selected=self.selected+1;
		console.log(self.selected);
		//$scope.step1.completed = true;
		//step1.completed=true;
	}


	//-------Getting Country List

	self.getCountryList = function () {
		try {
			$vendorFactories.getCountryList()
				.then(function successCallBack(response) {
					console.log(response.data);
					self.countryList = response.data;
					console.log(self.countryList);
					//console.log(self.countryList);
					//console.log(self.vendorDetails.countryList);
				},function errorCallBack(response) {
					console.log(response);
				})

		} catch (e) {
				console.log(e);
		}

	}
	self.getCountryList();

	self.changeCountry = function(countryId){
		if(self.vendorDetails.state!=null){
			self.vendorDetails.state = undefined;
			$scope.residentialForm.state.$setUntouched();
		}
		self.getStateList(countryId)
	}
	//---------Get State List
	self.getStateList = function (countryId) {
		// $scope.vendorDetails.state=undefined;
		// residentialForm.state.$setUntouched();
		console.log(countryId);
		try {
			var payload = {
				"p_country_id":countryId
			}
			console.log(payload);
			$vendorFactories.getStateList(payload)
				.then(function successCallBack(response) {
					console.log(response);
					self.stateList = response.data;
					console.log(self.stateList);
					//console.log(self.countryList);
					//console.log(self.vendorDetails.countryList);
				},function errorCallBack(response) {
					console.log(response.status+response.text);
				})

		} catch (e) {
				console.log(e);
		}

	}
	self.getCountryList();


	self.getGSTClass = function () {
		try {
			$vendorFactories.getGSTClass()
				.then(function successCallBack(response) {
					console.log(response.data);
					self.getGstClassList = response.data;
					console.log(self.getGstClassList);
				},function errorCallBack(response) {
					console.log(response.status+response.text);
				})
		} catch (e) {
					console.log(e);
		}
	}
	self.getGSTClass();

	self.setVendorDetails = function () {

			var payload ={
				"p_vendor_name": self.vendorDetails.vendorName,
               	"p_title": self.vendorDetails.title,
                "p_name1": self.vendorDetails.nameOne,
                "p_name2": self.vendorDetails.nameTwo,
                "p_country_id": self.vendorDetails.country,
                "p_state_id": self.vendorDetails.state,
                "p_city": self.vendorDetails.branchCity,
                "p_street": self.vendorDetails.branchStreet,
                "p_house_no": self.vendorDetails.houseNumber,
                "p_postal_address": self.vendorDetails.postalcode,
                "p_mobile_no": self.vendorDetails.mobileNumber.toString(),
                "p_telephone_no": self.vendorDetails.telephoneNumber.toString(),
                "p_fax_no": self.vendorDetails.faxNumber.toString(),
                "p_bank_name":self.vendorDetails.bankName,
                "p_account_no": self.vendorDetails.bankAccountNumber,
                "p_holder_name": self.vendorDetails.accountHolderName,
                "p_ifsc": self.vendorDetails.ifscSwift.toString(),
                "p_branch_name": self.vendorDetails.branchName,
                "p_bank_city": self.vendorDetails.city,
                "p_bank_street": self.vendorDetails.street,
                "p_registration_no": self.vendorDetails.gstRegNumber==undefined?null:self.vendorDetails.gstRegNumber.toString(),
                "p_gst_class_id": self.vendorDetails.gstClass==undefined?null:self.vendorDetails.gstClass,
                "p_pan_no": self.vendorDetails.panNumber==undefined?null:self.vendorDetails.panNumber.toString(),
                "p_federal_id": self.vendorDetails.federalId==undefined?null:self.vendorDetails.federalId.toString(),
                "p_ssn":self.vendorDetails.ssn==undefined?null:self.vendorDetails.ssn.toString()
}
		console.log(payload);
			try {
				$vendorFactories.setVendorDetails(payload)
					.then(function successCallBack(response) {
						console.log(response);
						// var toastText = "Request #"+response.data.vendorId +" is Submitted";
						// $mdToast.show($mdToast.simple().textContent(toastText).action('OK').highlightAction(true).hideDelay(4000).highlightClass('md-accent'));
						$mdDialog.show(
						 $mdDialog.alert()
						   .parent(angular.element(document.querySelector('#popupContainer')))
						   .clickOutsideToClose(true)
						   //.title('This is an alert title')
						   .textContent("Request # "+response.data.vendorId +" is Submitted.")
						   .ariaLabel('Alert Dialog Demo')
						   .ok('OK')
						   //.targetEvent(ev)
					   );
						self.disableAll = true;

					},function errorCallBack(response) {
						console.log(response.status+response.text);
					})
			} catch (e) {
						console.log(e);
			}
	}

});
